-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 11:36 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `penjualan`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_cart`
--

CREATE TABLE IF NOT EXISTS `t_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cust` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_cart`
--

INSERT INTO `t_cart` (`id`, `id_cust`, `id_item`, `unit`) VALUES
(3, 1, 1, 74);

-- --------------------------------------------------------

--
-- Table structure for table `t_m_customer`
--

CREATE TABLE IF NOT EXISTS `t_m_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `disc` double NOT NULL,
  `type_disc` char(15) NOT NULL,
  `ktp_image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_m_item`
--

CREATE TABLE IF NOT EXISTS `t_m_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `unit` char(15) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` double NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_m_item`
--

INSERT INTO `t_m_item` (`id`, `name`, `unit`, `stock`, `price`, `image`) VALUES
(1, 'Nutrijal', 'pcs', 100, 1500, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_sales`
--

CREATE TABLE IF NOT EXISTS `t_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(15) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cust` int(11) NOT NULL,
  `list_id_item` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_disc` double NOT NULL,
  `total_price` double NOT NULL,
  `total_pay` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
